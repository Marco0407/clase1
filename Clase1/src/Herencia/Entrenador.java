/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author MAFAR
 */
public class Entrenador extends SeleccionFutbol{
    String idFederacion;

    public Entrenador(String idFederacion, int id, String nombre, String apellido, int edad) {
        super(id, nombre, apellido, edad);
        this.idFederacion = idFederacion;
    }
    
    public String getAtributos() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellido:" + apellido
                + "\nEdad: " + edad
                + "\nId Federacion: " + idFederacion;
    }

    //polimorfismo
    public void tipoSeleccionFutbol() {
        System.out.println("es un Entrenador");
    }

 
}

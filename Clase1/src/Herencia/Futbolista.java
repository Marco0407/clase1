/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author MAFAR
 */
public class Futbolista extends SeleccionFutbol{
    
    int dorsal;
    String demarcacion;

    public Futbolista(int id, String nombre, String apellido, int edad,int dorsal,String demarcacion) {
        super(id, nombre, apellido, edad);
        this.dorsal = dorsal;
        this.demarcacion = demarcacion;
    }
    
    public String getAtributos() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellido:" + apellido
                + "\nEdad: " + edad
                + "\nDorsal: " + dorsal
                + "\nDemarcacion: " + demarcacion;
    }

    //polimorfismo
    public void tipoSeleccionFutbol() {
        System.out.println("es un futbolista");
    }
}

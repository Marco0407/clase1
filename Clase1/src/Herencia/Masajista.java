/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author MAFAR
 */
public class Masajista extends SeleccionFutbol{

    String titulacion;
    int aniosExperencia;

    public Masajista(String titulacion, int aniosExperencia, int id, String nombre, String apellido, int edad) {
        super(id, nombre, apellido, edad);
        this.titulacion = titulacion;
        this.aniosExperencia = aniosExperencia;
    }
    
    public String getAtributos() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellido:" + apellido
                + "\nEdad: " + edad
                + "\nTitulacion: " + titulacion
                + "\nAños de experiencia: " + aniosExperencia;
    }

    //polimorfismo
    public void tipoSeleccionFutbol() {
        System.out.println("es un Masajista");
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author MAFAR
 */
public class SeleccionFutbol {
    public int id;
    public String nombre;
    public String apellido;
    public int edad;

    public SeleccionFutbol(int id, String nombre, String apellido, int edad) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }
    
    public String getAtributos() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellido:" + apellido
                + "\nEdad: " + edad;
    }
    
    
    //polimorfismo
    public void tipoSeleccionFutbol() {
        System.out.println("Qué es?");
    }
}

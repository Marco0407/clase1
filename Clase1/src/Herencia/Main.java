/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author MAFAR
 */
public class Main {
    public static void main(String[] args) {
        
        Futbolista ju1 = new Futbolista(1, "Marco", "Ruiz", 19, 7, "Delantero");
        Masajista ma1 = new Masajista("Doctorado", 2, 2, "Johnny", "Mendez", 20);
        Entrenador en1 = new Entrenador("45", 32, "Gerardo", "Perez", 50);
        
        System.out.println(ju1.getAtributos());
        tipo(ju1);
        System.out.println("");
        
        System.out.println(ma1.getAtributos());
        tipo(ma1);
        System.out.println("");
        
        System.out.println(en1.getAtributos());
        tipo(en1);
        System.out.println("");
        
    }
    
    //polimorfismo
    public static void tipo (SeleccionFutbol seleccionFutbol){
        seleccionFutbol.tipoSeleccionFutbol();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

/**
 *
 * @author Johnny
 */
public class pCongelados extends SubPrincipal {

    String fechaEnvasados;
    String paisOrigen;
    String temRecomendada;

    public pCongelados(String fechaEnvasados, String paisOrigen, String temRecomendada, String fechaCaducidad, int numeroLote) {
        super(fechaCaducidad, numeroLote);
        this.fechaEnvasados = fechaEnvasados;
        this.paisOrigen = paisOrigen;
        this.temRecomendada = temRecomendada;
    }

    public String getAtributos() {
        return "Fecha envsados: " + fechaEnvasados
                + "\nPais Origen: " + paisOrigen
                + "\nTemperatura recomendada: " + temRecomendada
                + "\nFecha de caducidad: " + fechaCaducidad
                + "\nNumero de lote: " + numeroLote;

    }
}
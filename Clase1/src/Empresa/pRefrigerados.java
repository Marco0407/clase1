/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

/**
 *
 * @author Johnny
 */
public class pRefrigerados extends SubPrincipal {

    int codOrganismo;
    String fechaEnvasado;
    String temRecomendada;
    String paisOrigen;

    public pRefrigerados(int codOrganismo, String fechaEnvasado, String temRecomendada, String paisOrigen, String fechaCaducidad, int numeroLote) {
        super(fechaCaducidad, numeroLote);
        this.codOrganismo = codOrganismo;
        this.fechaEnvasado = fechaEnvasado;
        this.temRecomendada = temRecomendada;
        this.paisOrigen = paisOrigen;
    }

    public String getAtributos() {
        return "Codigo de organismos: " + codOrganismo
                + "\nFecha envasado: " + fechaEnvasado
                + "\nTemperatura recomendada " + temRecomendada
                + "\nPais Origen: " + paisOrigen
                + "\nFecha de caducidad: " + fechaCaducidad
                + "\nNumero de lote: " + numeroLote;

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

/**
 *
 * @author Johnny
 */
public class Principal {

    public static void main(String[] args) {
        Logica log = new Logica();
        SubPrincipal empre1 = new SubPrincipal("2014", 123);

        pFresco emFresco1 = new pFresco("2018", "CR", "2050", 1234);
        pRefrigerados eRefrigerados = new pRefrigerados(1, "2000", "10", "CR", "2020", 3);
        pCongelados eCongelados = new pCongelados("2015", "China", "20", "2019", 12);

        System.out.println(emFresco1.getAtributos());
        log.congelarAgua(12);
        System.out.println("");
        System.out.println(eRefrigerados.getAtributos());
        log.congelarNitrogeno(120, "como guste");
        System.out.println("");
        System.out.println(eCongelados.getAtributos());
        log.congelarAire(12, 12, 14, 17);

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

/**
 *
 * @author Johnny
 */
public class SubPrincipal {

    String fechaCaducidad;
    int numeroLote;

    public SubPrincipal(String fechaCaducidad, int numeroLote) {
        this.fechaCaducidad = fechaCaducidad;
        this.numeroLote = numeroLote;
    }

    public String getAtributos() {
        return "Fecha de caducidad: " + fechaCaducidad
                + "\nNumero de lote: " + numeroLote;
           
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa;

/**
 *
 * @author Johnny
 */
public class pFresco extends SubPrincipal {

    String fechaEnvasados;
    String paisOrigen;

    public pFresco(String fechaEnvasdo, String paisOrigen, String fechaCaducidad, int numeroLote) {
        super(fechaCaducidad, numeroLote);
        this.fechaEnvasados = fechaEnvasdo;
        this.paisOrigen = paisOrigen;
    }

    public String getAtributos() {
        return "Fecha envsados: " + fechaEnvasados
                + "\nPais Origen: " + paisOrigen
                + "\nFecha de caducidad: " + fechaCaducidad
                + "\nNumero de lote: " + numeroLote;

    }
}

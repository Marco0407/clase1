/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase1;

import javax.swing.JOptionPane;

/**
 *
 * @author MAFAR
 */
public class Logica {
    
    Persona[][] personas = new Persona[2][2];

    public Logica() {

    }

    public String numeroSuerte(int dia, int mes, int anno) {
        int suma;
        int suerte;
        int n1;
        int n2;
        int n3;
        int n4;
        suma = dia + mes + anno;
        n1 = suma / 1000;
        n2 = suma / 100 % 10;
        n3 = suma / 10 % 10;
        n4 = suma % 10;
        suerte = n1 + n2 + n3 + n4;
        return "El numero de la suerte es: " + suerte;
    }

    public String numeroEntero(int n) {
        int i = 0;
        int sumaP = 0;
        String tex = "";
        for (i = 1; i < n; i++) {
            if (n % i == 0) {
                sumaP = sumaP + i;
            }
        }
        if (sumaP == n) {
            tex = "Es perfecto";
        } else {
            tex = "No es perfecto";
        }
        return tex;
    }

    public void adivinarNumero() {
        int aleatorio = (int) (Math.random() * 10) + 1;
//        System.out.println(aleatorio);
        for (int i = 1; i < 6; i++) {
            JOptionPane.showMessageDialog(null, i + "/" + 5 + "Intentos", "INFORMACION", JOptionPane.INFORMATION_MESSAGE);

            int num = Integer.parseInt(JOptionPane.showInputDialog("Digite un numero del 1-10: "));

            if (num == aleatorio) {
                JOptionPane.showMessageDialog(null, "Felicidaddes has ganado: ", "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
                break;
            } else if (i == 5) {
                JOptionPane.showMessageDialog(null, "Has perdido ", "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
            } else if (num > aleatorio) {
                JOptionPane.showMessageDialog(null, "el numero adivinar es menor: ", "INFORMACION", JOptionPane.INFORMATION_MESSAGE);

            } else if (num < aleatorio) {
                JOptionPane.showMessageDialog(null, "el numero adivinar es mayor : ", "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
            }

        }
    }
        
        void guardarPersonas(String nombre, int edad) {
        int a = 0;
        int b = 0;
        for (int i = 0; i < personas.length; i++) {
            for (int j = 0; j < personas[i].length; j++) {
                if (personas[i][j] == null) {
                    a = i;
                    b = j;
                    break;
                }
            }
        }
        personas[a][b] = new Persona(nombre, edad);
    }

    boolean hayCampo() {
        boolean m = false;
        for (int i = 0; i < personas.length; i++) {
            for (int j = 0; j < personas[i].length; j++) {
                if (personas[i][j] == null) {
                    m = true;
                }
            }
        }
        return m;
    }

    void cambiarEdad(String nombre, int edad) {
        for (int i = 0; i < personas.length; i++) {
            for (int j = 0; j < personas[i].length; j++) {
                if (personas[i][j] != null && personas[i][j].getNombre().equals(nombre)) {
                    personas[i][j].setEdad(edad);
                }
            }
        }
    }

    boolean verificarPersona(String nombre) {
        boolean m = false;
        for (int i = 0; i < personas.length; i++) {
            for (int j = 0; j < personas[i].length; j++) {
                if (personas[i][j] != null && nombre.equals(personas[i][j].getNombre())) {
                    m = true;
                    break;
                }
            }
        }
        return m;
    }

    String imprimir() {
        String texto = "La lista de personas es:";
        for (int i = 0; i < personas.length; i++) {
            for (int j = 0; j < personas[i].length; j++) {
                if (personas[i][j] != null) {
                    texto += "\n" + personas[i][j].getNombre() + " ";
                    if (personas[i][j].getEdad() % 2 == 0) {
                        //azul
                        texto +=personas[i][j].getEdad();
                    } else {
                        //rojo
                        texto +=personas[i][j].getEdad();
                    }
                }
            }
        }
        return texto;
    }

    boolean vacia() {
        boolean m = true;
        for (int i = 0; i < personas.length; i++) {
            for (int j = 0; j < personas[i].length; j++) {
                if (personas[i][j] != null) {
                    m = false;
                    break;
                }
            }
        }
        return m;
    }
    }

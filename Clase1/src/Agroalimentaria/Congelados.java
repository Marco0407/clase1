/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agroalimentaria;

/**
 *
 * @author MAFAR
 */
public class Congelados extends Informacion{
    String fachaEnvazado;
    String paisOrigen;
    int temperatura;

    public Congelados(String fachaEnvazado, String paisOrigen, int temperatura, String fechaCaducidad, int lote) {
        super(fechaCaducidad, lote);
        this.fachaEnvazado = fachaEnvazado;
        this.paisOrigen = paisOrigen;
        this.temperatura = temperatura;
    }
    
}
